#!/usr/bin/env bash

PUBLIC_GPG_KEY_FILE="public-v3.gpg.asc"

set -eu

if [[ $(uname -s) == "Linux" ]] then
  [[ active != $(systemctl is-active pcscd) ]] && \
    systemctl enable --now pcscd || \
    systemctl restart pcscd
fi

[[ -f ${PUBLIC_GPG_KEY_FILE} ]] \
  && gpg --import ${PUBLIC_GPG_KEY_FILE} \
  && gpg-connect-agent "scd serialno" "learn --force" /bye
