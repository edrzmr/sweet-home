{
  description = "Flake for Scala Dev";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells.default = pkgs.mkShell {
          
          packages = [
            pkgs.coursier
            pkgs.postgresql_15
            # for pdfs
            pkgs.nodejs_18
            pkgs.yarn
          ];

          shellHook = ''
            echo '#compdef _cs cs

            function _cs {
              eval "$(cs complete zsh-v1 $CURRENT $words[@])"
            }' > ~/.zsh/completions/_cs
          '';

        };
      }

    );
}

