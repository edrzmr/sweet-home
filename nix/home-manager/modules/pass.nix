pkgs: {
  enable = true;
  package = pkgs.pass.withExtensions (exts: [
    exts.pass-otp
    exts.pass-update
    exts.pass-import
  ]);
}
