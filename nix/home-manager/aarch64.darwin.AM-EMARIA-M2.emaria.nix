{ config, pkgs, ... }: {

  home.username = "emaria";
  home.homeDirectory = "/Users/emaria";
  home.stateVersion = "24.05";

  home.sessionVariables = {
    _LOCAL_BREW_DIR = "/opt/homebrew";
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    btop
    htop
    tig
    tree
    pstree
    neofetch
    jq
    qpdf
    m-cli
    yq
    gnused
    unixtools.netstat

    kubectl
    kubernetes-helm
  ];

  home.file."Library/KeyBindings/DefaultKeyBinding.dict".source = ./files/darwin-keybinding/DefaultKeyBinding.dict;
  home.file.".config/git/config".source = ./files/git/config;
  home.file.".config/git/ignore".source = ./files/git/ignore;
  programs.vim = import ./modules/vim.nix pkgs;
  programs.zsh = import ./modules/zsh.nix pkgs;
  programs.direnv = import ./modules/direnv.nix pkgs;
  programs.password-store = import ./modules/pass.nix pkgs;
  programs.starship = import ./modules/starship.nix pkgs;

  ##############
  # ssh config #
  ##############
  home.file.".ssh/config".source = ./files/ssh/config;

  ##############
  # gpg config #
  ##############
  home.file.".gnupg/gpg.conf".source = ./files/gnupg/gpg.conf;
  home.file.".gnupg/gpg-agent.conf".source = ./files/gnupg/gpg-agent.conf;
  home.file.".gnupg/sshcontrol".source = ./files/gnupg/sshcontrol;
  home.file.".gnupg/scdaemon.conf".source = ./files/gnupg/scdaemon.conf;

  home.file.".zsh/completions/.placeholder".text = "";
}
