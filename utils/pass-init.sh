#!/usr/bin/env bash

set -x

pass init eder.rzmr@proton.me
pass git init
pass git remote add origin git@gitlab.com:edrzmr/PasswordStoreData.git
pass git fetch origin --prune
pass git branch --set-upstream-to=origin/master master
pass git reset --hard origin/master
